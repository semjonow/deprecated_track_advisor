class CreateFuels < ActiveRecord::Migration
  def change
    create_table :fuels do |t|
      t.integer :car_id,       :null => false
      t.string  :company_name, :null => false, :default => ""
      t.integer :value,        :null => false
      t.string  :unit,         :null => false, :default => ""
      t.integer :price,        :null => false
      t.string  :currency,     :null => false, :default => ""

      t.timestamps
    end

    add_foreign_key(:fuels, :cars, :dependent => :delete)
  end
end
