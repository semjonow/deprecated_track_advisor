class CreateMileages < ActiveRecord::Migration
  def change
    create_table :mileages do |t|
      t.integer :car_id, :null => false
      t.integer :value,  :null => true
      t.string  :unit,   :null => false, :default => ""

      t.timestamps
    end

    add_foreign_key(:mileages, :cars, :dependent => :delete)
  end
end
