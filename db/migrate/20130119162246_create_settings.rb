class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.integer :user_id,          :null => false
      t.string  :unit,             :null => false, :default => ""
      t.string  :currency,         :null => false, :default => ""
      t.string  :distance,         :null => false, :default => ""
      t.string  :post_to_facebook, :null => false, :default => ""
      t.string  :post_to_twitter,  :null => false, :default => ""

      t.timestamps
    end

    add_foreign_key(:settings, :users, :dependent => :delete)
  end
end
