class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :user_id,         :null => false
      t.string  :name,            :null => false, :default => ""
      t.string  :slug,            :null => false, :default => ""
      t.string  :engine_capacity, :null => false, :default => ""
      t.string  :engine_fuel,     :null => false, :default => ""
      t.string  :engine_power,    :null => false, :default => ""
      t.integer :year,            :null => false, :default => 0
      t.text    :description,     :null => false, :default => ""

      t.timestamps
    end

    add_index :cars, :slug, :unique => true
    add_foreign_key(:cars, :users, :dependent => :delete)
  end
end
