class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.integer :car_id,       :null => false
      t.string  :company_name, :null => false, :default => ""
      t.integer :price,        :null => false
      t.string  :currency,     :null => false, :default => ""
      t.text    :description,  :null => false, :default => ""

      t.timestamps
    end

    add_foreign_key(:services, :cars, :dependent => :delete)
  end
end
