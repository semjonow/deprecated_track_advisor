# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130119162246) do

  create_table "cars", :force => true do |t|
    t.integer  "user_id",                         :null => false
    t.string   "name",            :default => "", :null => false
    t.string   "slug",            :default => "", :null => false
    t.string   "engine_capacity", :default => "", :null => false
    t.string   "engine_fuel",     :default => "", :null => false
    t.string   "engine_power",    :default => "", :null => false
    t.integer  "year",            :default => 0,  :null => false
    t.text     "description",     :default => "", :null => false
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "cars", ["slug"], :name => "index_cars_on_slug", :unique => true

  create_table "fuels", :force => true do |t|
    t.integer  "car_id",                       :null => false
    t.string   "company_name", :default => "", :null => false
    t.integer  "value",                        :null => false
    t.string   "unit",         :default => "", :null => false
    t.integer  "price",                        :null => false
    t.string   "currency",     :default => "", :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "mileages", :force => true do |t|
    t.integer  "car_id",                     :null => false
    t.integer  "value"
    t.string   "unit",       :default => "", :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "providers", :force => true do |t|
    t.integer  "user_id",                      :null => false
    t.string   "name",         :default => "", :null => false
    t.string   "uid",          :default => "", :null => false
    t.string   "token",        :default => "", :null => false
    t.string   "token_secret", :default => "", :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "providers", ["user_id", "name", "uid"], :name => "index_providers_on_user_id_and_name_and_uid", :unique => true

  create_table "services", :force => true do |t|
    t.integer  "car_id",                       :null => false
    t.string   "company_name", :default => "", :null => false
    t.integer  "price",                        :null => false
    t.string   "currency",     :default => "", :null => false
    t.text     "description",  :default => "", :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "settings", :force => true do |t|
    t.integer  "user_id",                          :null => false
    t.string   "unit",             :default => "", :null => false
    t.string   "currency",         :default => "", :null => false
    t.string   "distance",         :default => "", :null => false
    t.string   "post_to_facebook", :default => "", :null => false
    t.string   "post_to_twitter",  :default => "", :null => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",             :default => "", :null => false
    t.string   "username",          :default => "", :null => false
    t.string   "name",              :default => "", :null => false
    t.string   "slug",              :default => "", :null => false
    t.string   "crypted_password",  :default => "", :null => false
    t.string   "password_salt",     :default => "", :null => false
    t.string   "persistence_token", :default => "", :null => false
    t.string   "perishable_token",  :default => "", :null => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["slug"], :name => "index_users_on_slug", :unique => true
  add_index "users", ["username"], :name => "index_users_on_username", :unique => true

  add_foreign_key "cars", "users", :name => "cars_user_id_fk", :dependent => :delete

  add_foreign_key "fuels", "cars", :name => "fuels_car_id_fk", :dependent => :delete

  add_foreign_key "mileages", "cars", :name => "mileages_car_id_fk", :dependent => :delete

  add_foreign_key "providers", "users", :name => "providers_user_id_fk", :dependent => :delete

  add_foreign_key "services", "cars", :name => "services_car_id_fk", :dependent => :delete

  add_foreign_key "settings", "users", :name => "settings_user_id_fk", :dependent => :delete

end
