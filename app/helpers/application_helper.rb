module ApplicationHelper
  def post_car_to_social(car)
    user = car.user
    link             = car_url(car.slug)
    front_of_message = "#{user.name} now tracks #{car.name}'#{car.year} (#{car.engine}) "
    post_message_with_link_to_social(front_of_message, link, user)
  end

  def post_fuel_to_social(fuel)
    car              = fuel.car
    user             = car.user
    link             = car_fuels_url(car.slug)
    front_of_message = "#{user.name} tucked #{car.name} with #{pluralize(value, unit)} #{car.engine_fuel} (~#{pluralize(price, currency)}) "
    post_message_with_link_to_social(front_of_message, link, user)
  end

  def post_service_to_social(service)
    car              = service.car
    user             = car.user
    link             = car_services_url(car.slug)
    front_of_message = "#{user.name} made #{car.name} service#{company_name.present? ? ' in ' + company_name : ''} in the amount of ~#{pluralize(price, currency)} "
    post_message_with_link_to_social(front_of_message, link, user)
  end

  def post_mileage_to_social(mileage)
    car              = mileage.car
    user             = car.user
    link             = car_mileages_url(car.slug)
    front_of_message = "#{user.name} marked a new mileage (~#{pluralize(value, unit)}) at #{car.name}"
    post_message_with_link_to_social(front_of_message, link, user)
  end

  def post_message_with_link_to_social(front_of_message, link, user)
    begin
      if !user.twitter.nil? && user.setting.post_to_twitter?
        message = "#{front_of_message.length > 120 ? "#{front_of_message[0...115]}... " : front_of_message}#{link}"
        user.post_to_twitter(message)
      end
    rescue
    end
    begin
      if !user.facebook.nil? && user.setting.post_to_facebook?
        message = "#{front_of_message}#{link}"
        user.post_to_facebook(message)
      end
    rescue
    end
  end
end
