module ServicesHelper
  def error_message_for_service_currency(service)
    error_message_on(service, :price, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Price ") ||
        error_message_on(service, :currency, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Currency ")
  end

  def services_chart_data(car, currency)
    service_prices_by_day = car.services.total_grouped_by_day(1.month.ago, currency)
    data = service_prices_by_day.map do |k, service|
      {
          :created_at => k,
          :price      => service.first.try(:price) || 0,
      }
    end
    data.to_json
  end
end
