module FuelsHelper
  def error_message_for_fuel_unit(fuel)
    error_message_on(fuel, :value, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Value ") ||
        error_message_on(fuel, :unit, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Unit ")
  end

  def error_message_for_fuel_currency(fuel)
    error_message_on(fuel, (fuel.new_record? ? :price_per_unit : :price), :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Price ") ||
        error_message_on(fuel, :currency, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Currency ")
  end

  def fuels_chart_data(car, currency, unit)
    fuel_prices_by_day = car.fuels.total_price_grouped_by_day(1.month.ago, currency)
    fuel_values_by_day = car.fuels.total_value_grouped_by_day(1.month.ago, unit)
    data = fuel_prices_by_day.map do |k, fuel|
      {
          :created_at => k,
          :price      => fuel.first.try(:price) || 0,
          :value      => fuel_values_by_day[k].first.try(:value) || 0
      }
    end
    data.to_json
  end
end
