module CarsHelper
  def error_message_for_engine(car)
    error_message_on(car, :engine_power, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Power ") ||
        error_message_on(car, :engine_fuel, :html_tag => "span", :css_class => "help-inline", :prepend_text => "Fuel ")
  end

  def total_fuels(car, unit)
    param_unit = Fuel::UNIT_TYPES.include?(unit)  ? unit : Fuel::UNIT_TYPES.first
    pluralize(car.total_fuels(param_unit), param_unit)
  end

  def total_services(car, currency)
    param_currency = Service::CURRENCY_TYPES.include?(currency)  ? currency : Service::CURRENCY_TYPES.first
    pluralize(car.total_services(param_currency), param_currency)
  end

  def total_mileages(car, distance)
    param_distance = Mileage::UNIT_TYPES.include?(distance) ? distance : Mileage::UNIT_TYPES.first
    pluralize(car.total_mileages(param_distance), param_distance)
  end

  def current_unit
    if params[:unit].present? && Fuel::UNIT_TYPES.include?(params[:unit])
      params[:unit]
    else
      logged_in? ? current_user.setting.unit : Fuel::UNIT_TYPES.first
    end
  end

  def current_currency
    if params[:currency].present? && Fuel::CURRENCY_TYPES.include?(params[:currency])
      params[:currency]
    else
      logged_in? ? current_user.setting.currency : Fuel::CURRENCY_TYPES.first
    end
  end

  def current_distance
    if params[:distance].present? && Mileage::UNIT_TYPES.include?(params[:distance])
      params[:distance]
    else
      logged_in? ? current_user.setting.distance : Mileage::UNIT_TYPES.first
    end
  end

  def reverted_current_unit
    unit_types = Fuel::UNIT_TYPES
    unit_types.reject{ |el|el==current_unit }.first
  end

  def reverted_current_currency
    currency_types = Fuel::CURRENCY_TYPES
    currency_types.reject{ |el|el==current_currency }.first
  end

  def reverted_current_distance
    distance_types = Mileage::UNIT_TYPES
    distance_types.reject{ |el|el==current_distance }.first
  end

  def car_chart_data(car, currency)
    car_prices_by_day     = car.total_price_grouped_by_day(1.month.ago, currency)
    fuel_prices_by_day    = car.fuels.total_price_grouped_by_day(1.month.ago, currency)
    service_prices_by_day = car.services.total_grouped_by_day(1.month.ago, currency)

    data = car_prices_by_day.map do |k, node|
      {
          :created_at    => k,
          :total_price   => node.first.try(:price) || 0,
          :fuel_price    => fuel_prices_by_day[k].nil? ? 0 : fuel_prices_by_day[k].first.try(:price),
          :service_price => service_prices_by_day[k].nil? ? 0 : service_prices_by_day[k].first.try(:price)
      }
    end
    data.to_json
  end
end
