module MileagesHelper
  def error_message_for_mileage(mileage)
    error_message_on(mileage, :value, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Value ") ||
        error_message_on(mileage, :unit, :html_tag => "span", :css_class => "help-inline pull-right", :prepend_text => "Unit ")
  end

  def mileages_chart_data(car, unit)
    mileage_values_by_day = car.mileages.total_grouped_by_day(1.month.ago, unit)
    data = mileage_values_by_day.map do |k, mileage|
      {
          :created_at => k,
          :value      => mileage_values_by_day[k].first.try(:value) || 0
      }
    end
    data.to_json
  end
end
