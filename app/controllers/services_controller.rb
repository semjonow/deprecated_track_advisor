class ServicesController < ApplicationController
  before_filter :check_logged_in, :except => [:index]

  def index
    @car      = Car.find(params[:car_id])
    @services = @car.services.order("id DESC")
  end

  def new
    @service = current_user.cars.find(params[:car_id]).services.build
  end

  def create
    @service = current_user.cars.find(params[:car_id]).services.build(params[:service])
    if @service.save
      flash[:notice] = "New service successfully saved"
      redirect_to car_services_path(@service.car)
    else
      render :new
    end
  end

  def destroy
    service = current_user.cars.find(params[:car_id]).services.find(params[:id])
    if service.destroy
      flash[:notice] = "Service successfully deleted"
    end
    redirect_to :back
  end
end
