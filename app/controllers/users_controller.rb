class UsersController < ApplicationController
  before_filter :check_logged_in

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update_attributes(params[:user])
      flash[:notice] = "Your profile successfully updated"
      redirect_to edit_user_path(current_user)
    else
      render :edit
    end
  end
end