class MileagesController < ApplicationController
  before_filter :check_logged_in, :except => [:index]

  def index
    @car      = Car.find(params[:car_id])
    @mileages = @car.mileages.order("id DESC")
  end

  def new
    @mileage = current_user.cars.find(params[:car_id]).mileages.build
  end

  def create
    @mileage = current_user.cars.find(params[:car_id]).mileages.build(params[:mileage])
    if @mileage.save
      flash[:notice] = "New mileage successfully saved"
      redirect_to car_mileages_path(@mileage.car)
    else
      render :new
    end
  end

  def destroy
    mileage = current_user.cars.find(params[:car_id]).mileages.find(params[:id])
    if mileage.destroy
      flash[:notice] = "Mileage successfully deleted"
    end
    redirect_to :back
  end
end
