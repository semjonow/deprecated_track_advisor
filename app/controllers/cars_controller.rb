class CarsController < ApplicationController
  before_filter :check_logged_in, :except => [:show]

  def new
    @car     = current_user.cars.build
    @mileage = @car.mileages.build if @car.current_mileage(true).nil?
    @fuel    = @car.fuels.build    if @car.last_fuel(true).nil?
    @service = @car.services.build if @car.last_service(true).nil?
  end

  def create
    @car = current_user.cars.build(params[:car])

    if @car.save
      flash[:notice] = "New car successfully saved."
      redirect_to car_path(@car)
    else
      @mileage = @car.current_mileage(true).nil? ? @car.mileages.build : @car.current_mileage(true)
      @fuel    = @car.last_fuel(true).nil? ? @car.fuels.build : @car.last_fuel(true)
      @service = @car.last_service(true).nil? ? @car.services.build : @car.last_service(true)
      render :new
    end
  end

  def edit
    @car = current_user.cars.find(params[:id])
    @mileage = @car.current_mileage(true).nil? ? @car.mileages.build : @car.current_mileage(true)
    @fuel    = @car.last_fuel(true).nil? ? @car.fuels.build : @car.last_fuel(true)
    @service = @car.last_service(true).nil? ? @car.services.build : @car.last_service(true)
  end

  def update
    @car = current_user.cars.find(params[:id])
    params[:car].delete(:mileages_attributes)

    if @car.update_attributes(params[:car])
      flash[:notice] = "Car successfully updated."
      redirect_to car_path(@car)
    else
      @mileage = @car.current_mileage(true).nil? ? @car.mileages.build : @car.current_mileage(true)
      @fuel    = @car.last_fuel(true).nil? ? @car.last_fuel.build : @car.last_fuel(true)
      @service = @car.last_service(true).nil? ? @car.last_service.build : @car.last_service(true)
      render :edit
    end
  end

  def destroy
    car = current_user.cars.find(params[:id])
    if car.destroy
      flash[:notice] = "Car successfully deleted."
    end
    redirect_to cars_path
  end

  def index
    if current_user.cars.count == 1
      @car = current_user.cars.first
      render :show
    else
      @cars = current_user.cars.all
    end
  end

  def show
    @car = Car.find(params[:id])
  end
end
