class FuelsController < ApplicationController
  before_filter :check_logged_in, :except => [:index]

  def index
    @car   = Car.find(params[:car_id])
    @fuels = @car.fuels.order("id DESC")
  end

  def new
    @fuel = current_user.cars.find(params[:car_id]).fuels.build
  end

  def create
    @fuel = current_user.cars.find(params[:car_id]).fuels.build(params[:fuel])
    if @fuel.save
      flash[:notice] = "New fueling successfully saved"
      redirect_to car_fuels_path(@fuel.car)
    else
      render :new
    end
  end

  def destroy
    fuel = current_user.cars.find(params[:car_id]).fuels.find(params[:id])
    if fuel.destroy
      flash[:notice] = "Fueling successfully deleted"
    end
    redirect_to :back
  end
end
