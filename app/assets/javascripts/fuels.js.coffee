jQuery ->
  if $("#fuels-chart").size() > 0 && $("#annual").size() > 0
    Morris.Line
      element: "annual"
      data:    $("#fuels-chart").data("fuels")
      xkey:    "created_at"
      ykeys:   ["price", "value"]
      labels:  ["Price (" + $("#fuels-chart").data("currency") + ")", "Value (" + $("#fuels-chart").data("unit") + ")"]
      # barColors: (row, series, type) ->
      #  if (type == 'bar')
      #    red = Math.ceil(120 * row.y / this.ymax)
      #    return 'rgb(' + red + ',0,0)'
      #  else
      #    return '#000'