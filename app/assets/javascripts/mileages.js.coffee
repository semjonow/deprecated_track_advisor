jQuery ->
  $(".btn-radio").click (e)->
    $(@).parent().find(".elem-radio").removeAttr("checked")
    $(@).next().attr("checked", "checked")

  if $(".btn-radio")
    if $(".elem-radio:checked")
      $(".elem-radio:checked").prev().click()
    else
      $(".btn-radio").first().click()

  if $("#mileages-chart").size() > 0 && $("#annual").size() > 0
    Morris.Line
      element: "annual"
      data:    $("#mileages-chart").data("mileages")
      xkey:    "created_at"
      ykeys:   ["value"]
      labels:  ["Value (" + $("#mileages-chart").data("unit") + ")"]