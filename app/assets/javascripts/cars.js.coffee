jQuery ->
  $("#car_year, #car_engine_fuel").select2()

  if $("#car-chart").size() > 0 && $("#annual").size() > 0
    Morris.Line
      element: "annual"
      data:    $("#car-chart").data("car")
      xkey:    "created_at"
      ykeys:   ["total_price", "service_price", "fuel_price"]
      labels:  ["Total Price (" + $("#car-chart").data("currency") + ")", "Service Price (" + $("#car-chart").data("currency") + ")", "Fuel Price (" + $("#car-chart").data("currency") + ")"]