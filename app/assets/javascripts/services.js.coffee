jQuery ->
  if $("#services-chart").size() > 0 && $("#annual").size() > 0
    Morris.Line
      element: "annual"
      data:    $("#services-chart").data("services")
      xkey:    "created_at"
      ykeys:   ["price"]
      labels:  ["Price (" + $("#services-chart").data("currency") + ")"]