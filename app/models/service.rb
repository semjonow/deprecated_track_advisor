class Service < ActiveRecord::Base
  include ApplicationHelper
  include ActionView::Helpers::TextHelper
  include Rails.application.routes.url_helpers

  CURRENCY_TYPES = ["USD", "EUR"]

  belongs_to :car, :touch => true

  attr_accessible :company_name, :price, :currency, :description

  after_initialize do
    if new_record?
      self.currency = User.current.setting.currency unless self.currency.present?
    end
  end

  validates :price,    :presence => true, :numericality => { :only_integer => true }
  validates :currency, :presence => true, :inclusion => { :in => Service::CURRENCY_TYPES }

  after_create :post_to_social

  scope :in_eur, where(:currency => "EUR")
  scope :in_usd, where(:currency => "USD")

  def self.to_eur(usd)
    Money.new(usd, "USD").exchange_to("EUR").fractional.round
  end

  def self.to_usd(eur)
    Money.new(eur, "EUR").exchange_to("USD").fractional.round
  end

  def self.total_grouped_by_day(start, param_currency = CURRENCY_TYPES.first)
    services = where(:created_at => start.beginning_of_day..Time.current)
    services = services.group("date(created_at)")
    case_condition = param_currency == "EUR" ? "CASE WHEN currency='EUR' THEN price ELSE price / #{EU_BANK.get_rate('EUR', 'USD')} END" \
      : "CASE WHEN currency='USD' THEN price ELSE price * #{EU_BANK.get_rate('EUR', 'USD')} END"
    services = services.select("date(created_at) as created_at, round(sum(#{case_condition})) as price")
    services.group_by{ |f| f.created_at.to_date }
  end

  protected

  def post_to_social
    post_service_to_social(self)
  end
end
