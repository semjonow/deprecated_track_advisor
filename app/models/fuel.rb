class Fuel < ActiveRecord::Base
  include ApplicationHelper
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::TextHelper

  UNIT_TYPES     = ["Gallon", "Liter"]
  CURRENCY_TYPES = ["USD", "EUR"]

  belongs_to :car, :touch => true

  attr_accessible :value, :unit, :price_per_unit, :currency
  attr_accessor   :price_per_unit

  after_initialize do
    if new_record?
      self.unit     = User.current.setting.unit     unless self.unit.present?
      self.currency = User.current.setting.currency unless self.currency.present?
    end
  end

  validates :value,          :presence => true, :numericality => { :only_integer => true }
  validates :price_per_unit, :presence => true, :numericality => true
  validates :price,          :presence => true, :numericality => { :only_integer => true }
  validates :unit,           :presence => true, :inclusion => Fuel::UNIT_TYPES
  validates :currency,       :presence => true, :inclusion => Fuel::CURRENCY_TYPES

  before_validation :calculate_total_price
  after_create :post_to_social

  scope :in_liters,  where(:unit => "Liter")
  scope :in_gallons, where(:unit => "Gallon")

  def self.to_gallons(liters)
    (liters * 0.264172).round
  end

  def self.to_liters(gallons)
    (gallons / 0.264172).round
  end

  def self.to_eur(usd)
    Money.new(usd, "USD").exchange_to("EUR").fractional.round
  end

  def self.to_usd(eur)
    Money.new(eur, "EUR").exchange_to("USD").fractional.round
  end

  def self.total_price_grouped_by_day(start, param_currency = CURRENCY_TYPES.first)
    fuels = where(:created_at => start.beginning_of_day..Time.current)
    fuels = fuels.group("date(created_at)")
    case_condition = param_currency == "EUR" ? "CASE WHEN currency='EUR' THEN price ELSE price / #{EU_BANK.get_rate('EUR', 'USD')} END" \
      : "CASE WHEN currency='USD' THEN price ELSE price * #{EU_BANK.get_rate('EUR', 'USD')} END"
    fuels = fuels.select("date(created_at) as created_at, round(sum(#{case_condition})) as price")
    fuels.group_by{ |f| f.created_at.to_date }
  end

  def self.total_value_grouped_by_day(start, param_unit = UNIT_TYPES.first)
    fuels = where(:created_at => start.beginning_of_day..Time.current)
    fuels = fuels.group("date(created_at)")
    case_condition = param_unit == "Liter" ? "CASE WHEN unit='Liter' THEN value ELSE value / 0.264172 END" \
      : "CASE WHEN unit='Gallon' THEN value ELSE value * 0.264172 END"
    fuels = fuels.select("date(created_at) as created_at, round(sum(#{case_condition})) as value")
    fuels.group_by{ |f| f.created_at.to_date }
  end

  protected

  def calculate_total_price
    self.price = (self.price_per_unit.to_f * self.value.to_f).round
  end

  protected

  def post_to_social
    post_fuel_to_social(self)
  end
end
