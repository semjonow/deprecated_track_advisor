class Car < ActiveRecord::Base
  include ApplicationHelper
  include Rails.application.routes.url_helpers

  extend FriendlyId
  friendly_id :name, :use => :slugged

  FUEL_TYPES = ["Petrol", "Diesel", "Petrol + Gas (LPG)", "Petrol + Gas (CNG)", "Gas (LPG)", "Gas (CNG)", "Hybrid", "Ethanol"]

  belongs_to :user
  has_many   :mileages
  has_many   :fuels
  has_many   :services

  attr_accessible :name, :year, :description, :engine_capacity, :engine_power, :engine_fuel, :mileages_attributes, :fuels_attributes, :services_attributes
  accepts_nested_attributes_for :mileages, :reject_if => Proc.new{ |attr| attr[:value].blank? }
  accepts_nested_attributes_for :fuels,    :reject_if => Proc.new{ |attr| attr[:value].blank? && attr[:price].blank? }
  accepts_nested_attributes_for :services, :reject_if => Proc.new{ |attr| attr[:value].blank? && attr[:price].blank? }

  validates :name, :presence => true
  validates :year, :presence => true, :date => { :after => Proc.new{ Time.now - 60.years } }
  validates :engine_fuel,  :presence => true, :inclusion => Car::FUEL_TYPES

  after_create :post_to_social

  def engine
    output =  engine_capacity.present? ? engine_capacity : ""
    output += output.present? ? ", #{engine_fuel}" : engine_fuel   if engine_fuel.present?
    output += output.present? ? ", #{engine_power}" : engine_power if engine_power.present?
    output
  end

  def current_mileage(as_object = false)
    if (last_mileage = mileages.last).nil?
      as_object ? nil : ""
    else
      as_object ? last_mileage : "#{last_mileage.value} #{last_mileage.unit}"
    end
  end

  def last_fuel(as_object = false)
    if (last_fuel = fuels.last).nil?
      as_object ? nil : ""
    else
      as_object ? last_fuel : "#{last_fuel.value} #{last_fuel.unit} (#{last_fuel.price last_fuel.currency})"
    end
  end

  def last_service(as_object = false)
    if (last_service = services.last).nil?
      as_object ? nil : ""
    else
      as_object ? last_service : "#{last_service.price last_service.currency}"
    end
  end

  def total_fuels(unit_of_volume)
    if unit_of_volume == "Liter"
      fuels.in_liters.sum(&:value) + Fuel.to_liters(fuels.in_gallons.sum(&:value))
    else
      fuels.in_gallons.sum(&:value) + Fuel.to_gallons(fuels.in_liters.sum(&:value))
    end
  end

  def total_mileages(distance_unit)
    last_mileage = mileages.last
    first_mileage = mileages.first
    return 0 if last_mileage.nil? || first_mileage.nil?
    if distance_unit == "KM"
      (last_mileage.unit == "KM" ? last_mileage.value : Mileage.to_kms(last_mileage.value)) - (first_mileage.unit == "KM" ? first_mileage.value : Mileage.to_kms(first_mileage.value))
    else
      (last_mileage.unit == "Mile" ? last_mileage.value : Mileage.to_miles(last_mileage.value)) - (first_mileage.unit == "Mile" ? first_mileage.value : Mileage.to_miles(first_mileage.value))
    end
  end

  def total_services(currency)
    if currency == "EUR"
      services.in_eur.sum(&:price) + Service.to_eur(services.in_usd.sum(&:price))
    else
      services.in_usd.sum(&:price) + Service.to_usd(services.in_eur.sum(&:price))
    end
  end

  def owner_setting(name)
    user.setting.send(name)
  end

  def total_price_grouped_by_day(start, param_currency = Fuel::CURRENCY_TYPES.first)
    car_fuels    = fuels.total_price_grouped_by_day(start, param_currency)
    car_services = services.total_grouped_by_day(start, param_currency)

    car_fuels.map do |k, fuel|
      unless car_services[k].nil?
        car_fuels[k].first.price += car_services[k].first.price
        car_services.delete(k)
      end
    end

    car_services.map do |k, service|
      car_fuels[k] = service
    end

    car_fuels
  end

  protected

  def post_to_social
    post_car_to_social(self)
  end
end