class Setting < ActiveRecord::Base
  belongs_to :user

  after_initialize do
    if new_record?
      self.unit     = Fuel::UNIT_TYPES.first     unless self.unit.present?
      self.currency = Fuel::CURRENCY_TYPES.first unless self.currency.present?
      self.distance = Mileage::UNIT_TYPES.first  unless self.distance.present?
      self.post_to_facebook = "NO" unless self.post_to_facebook.present?
      self.post_to_twitter  = "NO" unless self.post_to_twitter.present?
    end
  end

  attr_accessible :unit, :currency, :distance, :post_to_facebook, :post_to_twitter

  validates :unit,     :presence => true, :inclusion => { :in => Fuel::UNIT_TYPES }
  validates :currency, :presence => true, :inclusion => { :in => Fuel::CURRENCY_TYPES }
  validates :distance, :presence => true, :inclusion => { :in => Mileage::UNIT_TYPES }
  validates :post_to_facebook, :presence => true, :inclusion => { :in => ["YES", "NO"] }
  validates :post_to_twitter,  :presence => true, :inclusion => { :in => ["YES", "NO"] }

  def post_to_twitter?
    return true if self.post_to_twitter == "YES"
    false
  end

  def post_to_facebook?
    return true if self.post_to_facebook == "YES"
    false
  end
end
