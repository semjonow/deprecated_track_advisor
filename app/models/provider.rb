class Provider < ActiveRecord::Base
  belongs_to :user, :touch => true

  attr_accessible :name, :uid, :token, :token_secret
end
