class Mileage < ActiveRecord::Base
  include ApplicationHelper
  include ActionView::Helpers::TextHelper
  include Rails.application.routes.url_helpers

  UNIT_TYPES = ["Mile", "KM"]

  belongs_to :car, :touch => true

  attr_accessible :value, :unit

  validates :value, :presence => true, :numericality => { :only_integer => true }
  validates :unit,  :presence => true, :inclusion => Mileage::UNIT_TYPES

  after_initialize do
    if new_record?
      self.unit = User.current.setting.distance unless self.unit.present?
    end
  end

  after_validation :check_value
  after_create :post_to_social

  def self.to_kms(miles)
    (miles / 0.621371).round
  end

  def self.to_miles(kms)
    (kms * 0.621371).round
  end

  def self.total_grouped_by_day(start, param_unit = UNIT_TYPES.first)
    mileages = where(:created_at => start.beginning_of_day..Time.current)
    mileages = mileages.group("date(created_at)")
    case_condition = param_unit == "KM" ? "CASE WHEN unit='KM' THEN value ELSE value * 621371 END" \
      : "CASE WHEN unit='Mile' THEN value ELSE value / 0.621371 END"
    mileages = mileages.select("date(created_at) as created_at, round(sum(#{case_condition})) as value")
    mileages.group_by{ |f| f.created_at.to_date }
  end

  protected

  def check_value
    unless car.nil?
      last_mileage = car.current_mileage(true)

      if !last_mileage.nil? && value.present?
        if self.unit == last_mileage.unit
          last_mileage_value = last_mileage.value
        else
          last_mileage_value = last_mileage.unit == "KM" ? Mileage.to_miles(last_mileage.value) : Mileage.to_kms(last_mileage.value)
        end
        errors.add(:value, "must be greater than the last saved mileage (~#{pluralize(last_mileage_value, self.unit)})")  if value < last_mileage_value
      end
    end
  end

  def post_to_social
    post_mileage_to_social(self)
  end
end
