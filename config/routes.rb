TrackAdvisor::Application.routes.draw do
  resources :users
  match "logout"                  => "sessions#destroy",         :as => :logout, :via => :delete
  match "auth/:provider/redirect" => "authentications#redirect", :as => :auth_provider
  match "auth/facebook/callback"  => "authentications#facebook"
  match "auth/twitter/callback"   => "authentications#twitter"

  root :to => "home#index"

  resources :cars do
    resource :mileages, :only => [:new, :create, :index] do
      match ""            => "mileages#index", :via => :get
      match ":id/destroy" => "mileages#destroy", :as => :destroy, :via => :delete
    end
    resources :fuels,    :only => [:new, :create, :destroy, :index]
    resources :services, :only => [:new, :create, :destroy, :index]
  end
end
