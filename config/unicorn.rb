root = "/home/deployer/apps/track_advisor/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "/tmp/unicorn.track_advisor.sock"
worker_processes 2
timeout 30