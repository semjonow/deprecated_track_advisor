Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, FACEBOOK_APP_KEY, FACEBOOK_APP_SECRET, :scope => "email,publish_actions,offline_access"
  provider :twitter,  TWITTER_APP_KEY,  TWITTER_APP_SECRET
end