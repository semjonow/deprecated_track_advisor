Twitter.configure do |config|
  config.consumer_key    = TWITTER_APP_KEY
  config.consumer_secret = TWITTER_APP_SECRET
end